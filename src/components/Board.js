import React from 'react';

import Square from './Square'

class Board extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            squares: Array(9).fill(null),
            xIsNext: true,
            arr: [],
        }
    }

    handleClick(i) {
        if (!this.state.squares[i]) {
            const squares = this.state.squares.slice();
            squares[i] = this.state.xIsNext ? 'X' : 'O';
            this.setState({squares: squares, xIsNext: !this.state.xIsNext});
        }
    }

    renderSquare(i) {
      return (
        <Square value={this.state.squares[i]} onClick={() => this.handleClick(i)} />
      )
    }

    whoWin (winner) {
        return winner ? 'Winner is ' + winner : this.state.xIsNext ? 'Next player: X' : 'Next player: O'
    }

    hideModal = () => {
        this.setState({squares: Array(9).fill(null), xIsNext: true, arr: []});
    }
  
    render() {
        const winner = calculateWinner(this.state.squares, this.state.arr);
        let inc = 0;

        function calculateWinner(squares, arr) {
            const lines = [
                [0, 1, 2],
                [3, 4, 5],
                [6, 7, 8],
                [0, 3, 6],
                [1, 4, 7],
                [2, 5, 8],
                [0, 4, 8],
                [2, 4, 6],
            ];
            squares.map((item) => {
                if (!item) {
                    item = "."
                }
                return arr.push(item)
            })
            for (let i = 0; i < lines.length; i++) {
                const [a, b, c] = lines[i];
                if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
                    return squares[a];
                }
            }
            return null;
        }
    
        return (
            <div>
                <div className={winner && "winnerModal"}>
                    <div className="status">{this.whoWin(winner)}</div>
                    <button onClick={this.hideModal}>Reser game</button>
                </div>
                <div className="board-row">
                    {this.renderSquare(0)}
                    {this.renderSquare(1)}
                    {this.renderSquare(2)}
                </div>
                <div className="board-row">
                    {this.renderSquare(3)}
                    {this.renderSquare(4)}
                    {this.renderSquare(5)}
                </div>
                <div className="board-row">
                    {this.renderSquare(6)}
                    {this.renderSquare(7)}
                    {this.renderSquare(8)}
                </div>
                <div className="results">
                    {this.state.arr.map((item, index) => {
                        let linebrake = '';
                        return (
                            <div key={index+'-a'}>
                                {(index === 0 || index % 9 === 0) &&
                                    <div
                                        key={index +"-b" + inc++}
                                        className="count-results"
                                    >{inc}</div>
                                }
                                <div
                                    key={index+'-c'}
                                    className={`line ${linebrake} ${index % 3 === 0 && "line-break"}`}
                                >{item}</div>
                            </div>
                        )
                    })}
                </div>
            </div>
        );
    }
}
  
export default Board;