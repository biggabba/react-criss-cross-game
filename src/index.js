import React from 'react'
import ReactDOM from 'react-dom'
import './style.scss'

import Game from './components/Game';
  
ReactDOM.render(
    <Game />,
    document.getElementById('root')
);
  